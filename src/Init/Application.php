<?php

namespace JyKeruyun\Init;

use JyKeruyun\Kernel\Response;
use JyKeruyun\Keruyun\Keruyun;
use JyKeruyun\BasicService\BaseConfig;

/**
 * Class Application.
 */
class Application extends Keruyun
{
  use Response;
  use BaseConfig;
  
  public function __construct(array $config = [])
  {
    parent::__construct($this->initConfig($config));
  }
}
