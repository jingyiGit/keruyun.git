<?php
// 桌台模块
namespace JyKeruyun\Keruyun;

use JyKeruyun\Kernel\Http;

trait Table
{
  /**
   * 取桌台列表
   * https://open.keruyun.com/docs/zh/RceTEXQBzPVmqdQuv1ix.html
   *
   * @param array $ids
   * @return bool
   */
  public function getTableList($ids = [])
  {
    $url = $this->createURL('/open/v1/table/fetchTables');
    $res = Http::httpPostJson($url, [
      'ids'        => $ids,
    ]);
    return $this->handleReturn($res);
  }
}
