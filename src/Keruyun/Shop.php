<?php
// 门店模块
namespace JyKeruyun\Keruyun;

use JyKeruyun\Kernel\Http;

trait Shop
{
  /**
   * 获取门店 Token
   *
   * @param $shop_id
   * @return bool
   */
  public function getShopToken($shop_id)
  {
    $this->shop_id = $shop_id;
    $url           = $this->createURL('/open/v1/token/get');
    $res           = Http::httpGet($url);
    if ($res['code'] == 0) {
      $this->token = $res['result']['token'];
      return $this->token;
    }
    $this->setError($res);
    return false;
  }
  
  /**
   * 设置门店id && token
   *
   * @param $shop_id
   * @param $token
   */
  public function setShopToken($shop_id, $token)
  {
    [$this->shop_id, $this->token] = [$shop_id, $token];
  }
  
  /**
   * 获取门店营业时间
   *
   * @return false|mixed
   */
  public function getShopBusinessTime()
  {
    $url = $this->createURL('/open/v1/shop/fetchPeriod');
    $res = Http::httpPostJson($url);
    return $this->handleReturn($res);
  }
  
  /**
   * 查询门店营业状态
   *
   * @return false|mixed
   */
  public function getShopBusinessStatus()
  {
    $url = $this->createURL('/open/v1/shop/queryStatus');
    $res = Http::httpPostJson($url);
    return $this->handleReturn($res);
  }
  
  /**
   * 获取门店信息
   *
   * @return false|mixed
   */
  public function getShopInfo()
  {
    $url = $this->createURL('/open/v1/shop/shopdetails');
    $res = Http::httpPostJson($url);
    return $this->handleReturn($res);
  }
  
  /**
   * 获取门店列表
   * https://open.keruyun.com/docs/zh/JceTEXQBzPVmqdQuqli_.html
   *
   * @return false|mixed
   */
  public function getShopList()
  {
    $url = $this->createURL('/open/v1/shop/shoplist');
    $res = Http::httpPostJson($url);
    return $this->handleReturn($res);
  }
  
  /**
   * 门店设备列表查询接口
   * (下行接口)第三方合作方调用此接口，查询客如云门店下的设备列表。(此接口限制调用次数，同一门店每小时限一次。设备状态实时变更请接入门店设备状态推送接口。)
   *
   * @return false|mixed
   */
  public function getDeviceList()
  {
    $url = $this->createURL('/open/v1/shop/device/list');
    $res = Http::httpGet($url);
    return $this->handleReturn($res);
  }
}
