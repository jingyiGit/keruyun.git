<?php
// 菜品/分类模块

namespace JyKeruyun\Keruyun;

use JyKeruyun\Kernel\Http;

trait Dish
{
  /**
   * 取商品列表
   *
   * @param int $startId
   * @param int $pageNum
   * @return false|mixed
   */
  public function getGoodList($startId = 1, $pageNum = 1000)
  {
    $url = $this->createURL('/open/v1/cater/dish/dishMenu');
    $res = Http::httpPostJson($url, [
      'shopIdenty' => $this->shop_id,
      'startId'    => $startId,
      'pageNum'    => $pageNum,
    ]);
    return $this->handleReturn($res);
  }
  
  /**
   * 取商品列表，根据分类ID
   *
   * @param int $dishTypeId
   * @return false|mixed
   */
  public function getGoodListByCategory($dishTypeId)
  {
    $url = $this->createURL('/open/v1/cater/dish/dishNew');
    $res = Http::httpPostJson($url, [
      'dishTypeId' => $dishTypeId,
    ]);
    return $this->handleReturn($res);
  }
  
  /**
   * 查询商品，根据商品id
   *
   * @param array $ids
   * @return false|mixed
   */
  public function getGoodByIds($ids = [])
  {
    $url = $this->createURL('/open/v1/cater/dish/dishMenuByIds');
    $res = Http::httpPostJson($url, [
      'shopIdenty' => $this->shop_id,
      'ids'        => $ids,
    ]);
    return $this->handleReturn($res);
  }
  
  /**
   * 取菜单分类
   *
   * @return false|mixed
   */
  public function getCategory()
  {
    $res = Http::httpPostJson($this->createURL('/open/v1/cater/dish/category'));
    return $this->handleReturn($res);
  }
  
  /**
   * 取菜单分类，带父级
   *
   * @return false|mixed
   */
  public function getCategoryAll()
  {
    $res = Http::httpPostJson($this->createURL('/open/v1/cater/dish/categoryAll'));
    return $this->handleReturn($res);
  }
  
  /**
   * 批量修改门店菜品售卖数量
   * https://open.keruyun.com/docs/zh/_MeVEXQBzPVmqdQuiVno.html
   */
  public function batchEdit()
  {
  
  }
}
