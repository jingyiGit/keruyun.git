<?php
// 外卖模块
// http://open.keruyun.com/docs/zh/iMeYEXQBzPVmqdQufV3I.html?b=cater
namespace JyKeruyun\Keruyun;

use JyKeruyun\Kernel\Http;

trait Takeout
{
  private $orderInfo = [];        // 总订单信息
  private $shopInfo = [];         // 店铺信息
  private $productsInfo = [];     // 商品信息
  private $deliveryInfo = [];     // 配送信息
  private $paymentInfo = [];      // 支付信息
  private $customersInfo = [];    // 顾客信息
  private $discountDetails = [];  // 优惠明细
  
  public function setTakeoutOrder_orderInfo($orderInfo)
  {
    if (!$orderInfo['tpOrderId']) {
      return $this->setError('tpOrderId 第三方订单号不能为空');
    }
    if (!$orderInfo['createTime']) {
      $orderInfo['createTime'] = time();
    }
    
    $this->orderInfo = $orderInfo;
  }
  
  public function setTakeoutOrder_shopInfo($shopInfo)
  {
    $this->shopInfo = $shopInfo;
  }
  
  public function setTakeoutOrder_productsInfo($productsInfo)
  {
    $this->productsInfo = $productsInfo;
  }
  
  public function setTakeoutOrder_deliveryInfo($deliveryInfo)
  {
    $this->deliveryInfo = $deliveryInfo;
  }
  
  public function setTakeoutOrder_paymentInfo($paymentInfo)
  {
    $this->paymentInfo = $paymentInfo;
  }
  
  public function setTakeoutOrder_customersInfo($customersInfo)
  {
    $this->customersInfo = $customersInfo;
  }
  
  public function setTakeoutOrder_discountDetails($discountDetails)
  {
    $this->discountDetails = $discountDetails;
  }
  
  /**
   * 创建外卖订单
   *
   * @param $order_id
   * @return false|mixed
   */
  public function createTakeoutOrder($order_id)
  {
    $url = $this->createURL('/open/v1/takeout/order/create');
    $res = Http::httpPostJson($url, $this->orderData($order_id));
    if ($res['code'] == 0 && $res['apiMessage'] == null) {
      $this->error = null;
      return $res['result'];
    }
    $this->setError($res);
    return false;
  }
  
  public function orderData($order_id)
  {
    $order     = [
      'order_id'     => $order_id,
      'promise_time' => time() + 1200,
      'self_send'    => 2,
      'name'         => '刘先生',
      'phone'        => '18124439923',
      'actual'       => 1,
      'amount'       => 1,
      'discount'     => 0,
    ];
    $orderShop = [
      'created_at' => time(),
      'shop_id'    => 1000,
      'shop_info'  => [
        'name' => '雅苑对面糖人街',
      ],
      'packfee'    => 0,
      'discount'   => 0,
      'remarks'    => '加辣一点，谢谢',
      'actual'     => 1,
      'amount'     => 1,
    ];
    
    $orderGood = [
      'title'       => '土豆丝炒肉肉',
      'quantity'    => 1,
      'price'       => 1,
      'packfee'     => 0,
      'total_price' => 1,
    ];
    
    $data = [
      'tpOrderId'          => $order['order_id'],
      'createTime'         => $orderShop['created_at'],
      'peopleCount'        => 1,
      'status'             => 2,
      'remark'             => $orderShop['remarks'],
      
      // 店铺信息
      'shop'               => [
        'shopIdenty' => $this->shop_id,
        'tpShopId'   => '810511331',
        'shopName'   => $orderShop['shop_info']['name'],
      ],
      
      // 商品信息
      'products'           => [
        [
          'name'            => '土豆丝炒肉肉',
          'type'            => 0,
          'tpId'            => '1001',
          'quantity'        => 1,
          'price'           => 1,
          'packagePrice'    => 0,
          'packageQuantity' => 1,
          'totalFee'        => 1,
        ],
      ],
      
      // 配送信息
      'delivery'           => [
        'expectTime'       => $order['promise_time'],
        'deliveryParty'    => 1,
        'receiverName'     => $order['name'],
        'receiverPhone'    => $order['phone'],
        'delivererName'    => '果子',
        'delivererPhone'   => '13106068684',
        'delivererAddress' => '广东省揭阳揭东玉湖',
        'receiverGender'   => 1,
        'coordinateType'   => 2,
        'longitude'        => 116.237891,
        'latitude'         => 23.689796,
      ],
      
      // 支付信息
      'payment'            => [
        'deliveryFee'         => 0,
        'packageFee'          => $orderShop['packfee'],
        'discountFee'         => $orderShop['discount'],
        'shopDiscountFee'     => 0,
        'platformDiscountFee' => 0,
        'shopFee'             => $orderShop['actual'],
        'userFee'             => $orderShop['amount'] - $orderShop['discount'],
        'totalFee'            => $orderShop['amount'],
        'serviceFee'          => 0,
        'subsidies'           => 0,
        'payType'             => 2,
      ],
      
      // 是否打印:1打印，0不打印，为空默认打印类型为9：打印消费清单
      'isPrint'            => 0,
      
      // 8：后厨单，9：消费清单，15：标签;
      'printTemplateTypes' => [9, 15],
    ];
    // $data = json_encode($data);
    // dd($data);
    return $data;
    
    
    // 菜品列表
    foreach ($orderGoods as $orderGood) {
      $properties = [];
      $params     = json_decode($orderGood['params'], true);
      
      foreach ($params['specs'] as $spec) {
        $properties[] = ['id' => 0, 'name' => $spec['name'], 'type' => 4, 'reprice' => 0];
      }
      
      foreach ($params['tastes'] as $taste) {
        $properties[] = ['id' => 0, 'name' => $taste['name'], 'type' => 1, 'reprice' => bcmul($taste['price'], 100)];
      }
      
      $data['products'][] = [
        'name'            => $orderGood['title'],
        'tpId'            => 0,
        'quantity'        => $orderGood['quantity'],
        'price'           => bcmul($orderGood['price'], 100) - sumby($properties, 'reprice'),
        'packagePrice'    => bcmul($orderGood['packfee'], 100),
        'packageQuantity' => $orderGood['quantity'],
        'totalFee'        => bcmul($orderGood['total_price'], 100),
        'properties'      => $properties,
      ];
    }
    
    if ($_GET['debug']) {
      dd($data);
    }
    
    return $data;
  }
}
